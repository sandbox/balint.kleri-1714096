<?php

/**
 * @file tweet_id.inc
 * Plugin to provide an argument handler for a tweet identifier.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Tweet: ID'),
  'description' => t('Creates a tweet context from a tweet ID argument.'),
  'context' => 'drupalaton_tweets_tweet_id_context',
);

/**
 * Creates the tweet context using the argument.
 *
 * We need to load the tweet here, if the argument is valid, and create the context.
 * If the $empty variable is TRUE, an empty context must be returned.
 *
 * Notes:
 *  - Creating an empty context: ctools_context_create_empty($type), where the $type is
 *    the name of the context.
 *  - Creating a context: ctools_context_create($type, $data), where the $type is the
 *    name of the context, and the $data is the loaded tweet.
 *  - Use the drupalaton_tweets_get_tweet($id) method to load a particular tweet.
 */
function drupalaton_tweets_tweet_id_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  if ($empty) {
    return ctools_context_create_empty('tweet');
  }
  if (!is_numeric($arg)) {
    return FALSE;
  }

  $tweet = drupalaton_tweets_get_tweet($arg);
  if (!$tweet) {
    return FALSE;
  }

  return ctools_context_create('tweet', $tweet);
}

