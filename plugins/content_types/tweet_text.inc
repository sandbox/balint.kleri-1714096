<?php

/**
 * @file tweet_text.inc
 * Plugin to provide a tweet text content type.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Tweet text'),
  'description' => t('Text of the tweet.'),
  'required context' => new ctools_context_required(t('Tweet'), 'tweet'),
  'category' => t('Tweet'),
);

/**
 * Renders the content type.
 *
 * The method should build and return an object that has the folling properties:
 * module, title, delta, content.
 */
function drupalaton_tweets_tweet_text_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  $block = new stdClass();
  $block->module = 'tweet_text';
  $block->title = t('Tweet text');
  $block->delta = $context->argument;
  $block->content = check_markup($context->data['text'], 'filtered_html');

  return $block;
}

/**
 * Form builder; settings form for the content type.
 * (It should be implemented even if it's empty.)
 */
function drupalaton_tweets_tweet_text_content_type_edit_form($form, &$form_state) {
  return $form;
}

