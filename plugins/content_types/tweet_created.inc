<?php

/**
 * @file tweet_created.inc
 * Plugin to provide a tweet creation date content type.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Tweet creation date'),
  'description' => t('Creation date of the tweet.'),
  'required context' => new ctools_context_required(t('Tweet'), 'tweet'),
  'category' => t('Tweet'),
  'defaults' => array(
    'date_type' => 'medium',
  ),
);

/**
 * Renders the content type.
 *
 * The method should build and return an object that has the folling properties:
 * module, title, delta, content.
 */
function drupalaton_tweets_tweet_created_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  $block = new stdClass();
  $block->module = 'tweet_created';
  $block->title = t('Tweet created');
  $block->delta = $context->argument;
  $block->content = format_date(strtotime($context->data['created_at']), $conf['date_type']);

  return $block;
}

/**
 * Form builder; settings form for the content type.
 * (It should be implemented even if it's empty.)
 */
function drupalaton_tweets_tweet_created_content_type_edit_form($form, &$form_state) {
  $conf = &$form_state['conf'];

  $date_types = array();
  foreach (system_get_date_types() as $type) {
    $date_types[$type['type']] = $type['title'] . ': ' . format_date(REQUEST_TIME, $type['type']);
  }
  $form['date_type'] = array(
    '#title' => t('Date type'),
    '#description' => t('Select the date type for rendering the tweet creation date.'),
    '#type' => 'select',
    '#options' => $date_types,
    '#default_value' => $conf['date_type'],
  );

  return $form;
}

function drupalaton_tweets_tweet_created_content_type_edit_form_submit($form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

