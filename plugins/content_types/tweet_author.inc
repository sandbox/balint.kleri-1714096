<?php

/**
 * @file tweet_author.inc
 * Plugin to provide a tweet author content type.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Tweet author'),
  'description' => t('Author of the tweet.'),
  'required context' => new ctools_context_required(t('Tweet'), 'tweet'),
  'category' => t('Tweet'),
  'defaults' => array(
    'name_format' => 'screen_name',
    'link' => FALSE,
  ),
);

/**
 * Renders the content type.
 *
 * The method should build and return an object that has the folling properties:
 * module, title, delta, content.
 */
function drupalaton_tweets_tweet_author_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  $block = new stdClass();
  $block->module = 'tweet_author';
  $block->title = t('Tweet author');
  $block->delta = $context->argument;

  $author = $context->data['user'][$conf['name_format']];
  if (!empty($conf['link'])) {
    $author = l($author, 'http://twitter.com/' . $context->data['user']['screen_name']);
  }
  $block->content = $author;

  return $block;
}

/**
 * Form builder; settings form for the content type.
 * (It should be implemented even if it's empty.)
 */
function drupalaton_tweets_tweet_author_content_type_edit_form($form, &$form_state) {
  $conf = &$form_state['conf'];

  $form['name_format'] = array(
    '#title' => t('Name format'),
    '#description' => t('Select which format should be used for showing the name of the author.'),
    '#type' => 'radios',
    '#options' => array(
      'screen_name' => t('Screen name (Twitter @username)'),
      'name' => t('Real name'),
    ),
    '#default_value' => $conf['name_format'],
  );
  $form['link'] = array(
    '#title' => t('Link to Twitter profile'),
    '#description' => t('Check here to make to author name link to the Twitter profile.'),
    '#type' => 'checkbox',
    '#default_value' => $conf['link'],
  );

  return $form;
}

function drupalaton_tweets_tweet_author_content_type_edit_form_submit($form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

