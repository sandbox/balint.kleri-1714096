<?php

/**
 * @file tweet_avatar.inc
 * Plugin to provide a tweet avatar content type.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Tweet avatar'),
  'description' => t('Avatar of the tweet author.'),
  'required context' => new ctools_context_required(t('Tweet'), 'tweet'),
  'category' => t('Tweet'),
);

/**
 * Renders the content type.
 *
 * The method should build and return an object that has the folling properties:
 * module, title, delta, content.
 */
function drupalaton_tweets_tweet_avatar_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  $block = new stdClass();
  $block->module = 'tweet_avatar';
  $block->title = t('Tweet avatar');
  $block->delta = $context->argument;

  $image_parameters = array(
    'path' => $context->data['user']['profile_image_url'],
    'alt' => $context->data['user']['screen_name'],
    'title' => $context->data['user']['screen_name'],
  );
  $block->content = theme('image', $image_parameters);

  return $block;
}

/**
 * Form builder; settings form for the content type.
 * (It should be implemented even if it's empty.)
 */
function drupalaton_tweets_tweet_avatar_content_type_edit_form($form, &$form_state) {
  return $form;
}

