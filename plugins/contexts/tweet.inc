<?php

/**
 * @file tweet.inc
 * Plugin to provide a tweet context.
 *
 * Twitter API provides a resource for retrieving a single status (tweet).
 * This context is a response wrapped in a context object that can be utilized
 * by anything that accepts contexts.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Tweet'),
  'description' => t('A tweet object.'),
  'context' => 'drupalaton_tweets_context_create_tweet',
  'edit form' => 'drupalaton_tweets_context_tweet_settings_form',
  'defaults' => array('tweet_id' => ''),
  'keyword' => 'tweet',
  'context name' => 'tweet',
  'convert list' => 'drupalaton_tweets_context_tweet_convert_list',
  'convert' => 'drupalaton_tweets_context_tweet_convert',
);

/**
 * Creates the tweet context.
 *
 * There are three possible scenarios:
 *   1. The $empty variable is TRUE.
 *      In this case we have nothing to do, just need to return an empty context.
 *   2. The $conf variable is TRUE.
 *      The context is being created from configuration with hardcoded values. We have to
 *      acquire the data that we need based on the configuration -- so we must load the
 *      tweet here. The $data array contains the values that have been configured on the UI.
 *      In our example it can contain the tweet ID.
 *   3. The $conf variable is FALSE.
 *      This is the most common case. The context is being created by an argument or a
 *      relationship. The $data variable already contains the tweet data that we just need to
 *      wrap in a context object.
 *
 * Notes:
 *   - Creating a new context here: $context = new ctools_context($type).
 *   - You have to specify the name of this plugin: $context->plugin = 'tweet'.
 *     Very important: it should be identical with the context's name!
 *   - Other mandatory context object properties: data, title, argument.
 *   - Use the drupalaton_tweets_get_tweet($id) method to load a particular tweet.
 */
function drupalaton_tweets_context_create_tweet($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('tweet');
  $context->plugin = 'tweet';

  if ($empty) {
    return $context;
  }

  if ($conf) {
    $data = drupalaton_tweets_get_tweet($data['tweet_id']);
  }

  if (!empty($data)) {
    $context->data = $data;
    $context->title = t('Tweet by @user', array('@user' => $data['user']['screen_name']));
    $context->argument = $data['id'];
    return $context;
  }
  return FALSE;
}

/**
 * Form builder; settings form for the context.
 */
function drupalaton_tweets_context_tweet_settings_form($form, &$form_state) {
  $conf = &$form_state['conf'];
  $form['tweet_id'] = array(
    '#title' => t('Enter the ID of a tweet'),
    '#type' => 'textfield',
    '#default_value' => $conf['tweet_id'],
  );
  return $form;
}

/**
 * Provides a list of ways that this context can be converted to a string.
 *
 * This method should return with an array, where the keys are the available
 * keywords, and the values are descriptions.
 */
function drupalaton_tweets_context_tweet_convert_list() {
  return array(
    'text' => t('Text of the tweet'),
    'created' => t('Creation date of the tweet'),
    'author' => t('Author of the tweet'),
  );
}

/**
 * Converts a context keyword into a string.
 *
 * We have to return with a rendered value based on the $type, which is the
 * keyword. Think about this as token replacements. The whole context is
 * available to reach the data that we need.
 */
function drupalaton_tweets_context_tweet_convert($context, $type) {
  switch ($type) {
    case 'text':
      return check_plain($context->data['text']);
    case 'created':
      return $context->data['created_at'];
    case 'author':
      return check_plain($context->data['user']['screen_name']);
  }
}

